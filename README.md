# Peppermint Linux Custom tasksel

## Introduction

Welcome to our custom fork of the Devuan `tasksel` package, tailored for Peppermint Linux Mini ISOs.

## Compatibility Limitation

In Peppermint Linux Mini ISOs, there's a limitation you should be aware of: we currently do not support the installation of multiple desktop environments simultaneously due to compatibility considerations.

## Acknowledgments

We would like to express our heartfelt appreciation to the Devuan community and the Devuan project for their invaluable contributions. This project builds upon the strong foundation they have laid.

## Using tasksel during Installation

When you embark on the installation journey of Peppermint Linux using our installer, you'll have the opportunity to select your preferred desktop environment with the help of `tasksel`. Here's how:

1. **Launching the Installer**: Start by booting your system using the Peppermint Linux Mini ISO and following the on-screen instructions to initiate the installation process.

2. **Selecting Desktop Environments**: As you progress through the installation wizard, you'll reach a stage where you can choose the desktop environment that resonate with you. Feel free to pick one of the following options:

   - To install the **Peppermint Default Desktop**, simply select it.
   - For any other desktop environments you desire, select them as well.

3. **Completing the Installation**: Continue with the installation process, and rest assured that `tasksel` will work its magic to install the desktop environment you've chosen.

4. **Finishing Up**: After the installation is complete, you'll have your Peppermint Linux system configured exactly the way you envisioned it, with your selected desktop environmen in place.

Please keep in mind that our customized `tasksel` is designed for single desktop installations. Attempting to install multiple desktop environments concurrently may lead to compatibility issues. For the most seamless experience, we recommend selecting the desktop environment that align best with your preferences during the installation process.

## Download Peppermint Linux Mini ISOs

You can download the latest Peppermint Linux Mini ISOs from the following link: [Peppermint Linux Mini ISOs](http://68.233.46.81/nightly/PepMini/)

## License

This project is licensed under the GNU General Public License, version 3 (GPL-3.0) or any later version. For more detailed information, please consult the [LICENSE] file.

## Author

- Peppermint team 
- manuel rosa

We greatly value your feedback and contributions to this project. If you have any questions or require further assistance, please do not hesitate to reach out to us.
